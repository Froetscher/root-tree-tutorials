# Root-TTree Tutorial 

## Code Examples
* tree_example1.C: Create a root file with some branches, fill them
* tree_example2.C: Read generated rootfile, simple analyzer with histograms
* tree_example3.C: Create Tree with fixed size arrays
* tree_example4.C: Create Tree with variable size arrays
* tree_example5.C: Reading Branch with Arrays, some logic to find largest entry
* tree_example6.C: Writing Tree with vectors (*No need to define arrays with MAX SIZE!*)
* tree_example7.C: Reading Tree with vectors as branch

## Branch with Event Class
* I) MyEvent.h: Writing the Class that will later be used as an Event, make shared object file
* II) tree_example8.C: Writing Tree using MyEvent Class
* III) tree_example9.C: Reading Tree with MyEvent.h Class object
* IV) tree_example10.C: Writing Tree with vector\<myEvent> as a branch
* V) tree_example11.C: Read Tree with vector\<myEvent> as a branch

### I) Classdefinition for Events
Create a shared object file from a Class definition written to *MyEvent.h* using the command prompt in root: 
> root[0] .L MyEvent.h+
  
  *Note*: uses ACLiC to generate dictionaries.
Use of the generated *MyEvent.so* is demonstrated in the next example.

### II/III) Create/Read a tree using MyEvent Class
To run example eight/nine:
> root [0] .L MyEvent.so  
> root [1] .L tree_example8.C  
> root [2] tree_example8()

To inspect the output, assuming it's still the same root session:
> root[3] TFile f("myFile.root");   
> root[4] T->Show(4)  
> root[5] T->GetLeaf("px")->GetValue()    


### IV) Writing Tree with vector\<myEvent> as branch
These parts are required at the end of MyEvent.h for this example:
>\#include \<vector>  
>\#if defined(\__MAKECINT__) || defined(\__ROOTCLING__)  
>\#pragma link C++ class MyEvent+;  
>pragma link C++ class std::vector\<MyEvent>+;  
>endif

### V) Read Tree with vector\<myEvent> as branch
Had an error with this one that was solved by deleting myfile.root and MyEvent.so and rerunning:
>.L MyEvent.h++

## Source of treated Material

Reference: https://www.niser.ac.in/sercehep2017/notes/RootTutorial_TTree.pdf
