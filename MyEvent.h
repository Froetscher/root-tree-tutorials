#ifndef __MYEVENT__
#define __MYEVENT__
#include <iostream>
#include "TObject.h"

// Create a shared objet file, using the command in root .L MyEvent.h+
// that will create a MyEvent.so in the same directory with ACLiC to generate dictionaries
class MyEvent{
 public:
  MyEvent(){px=0;py=0;pt=0;};
  virtual ~MyEvent(){};

  void setPx(float x_) {px=x_;};
  void setPy(float y_) {py=y_;};
  void setPt(float t_) {pt=t_;};

  float getPx() {return px;};
  float getPy() {return py;};
  float getPt() {return pt;};

 private:
  float px;
  float py;
  float pt;
  ClassDef(MyEvent,1);

};
#endif

#include <vector>
#if defined(__MAKECINT__) || defined(__ROOTCLING__)
#pragma link C++ class MyEvent+;
#pragma link C++ class std::vector<MyEvent>+;
#endif
